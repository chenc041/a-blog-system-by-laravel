<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePostsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('posts', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('category_id')->default(1)->comment('分类 id');
			$table->string('title')->comment('标题');
			$table->string('summary', 1000)->comment('摘要');
			$table->text('content', 65535)->comment('内容');
			$table->integer('view_count')->nullable()->comment('浏览次数');
			$table->integer('status')->comment('状态，0：正常，1：删除');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('posts');
	}

}
