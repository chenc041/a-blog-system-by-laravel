<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

Auth::routes();

Route::namespace('Admin')->group(function () {
    // 在 "App\Http\Controllers\Admin" 命名空间下的控制器
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/edit/{id}', 'HomeController@edit')->name('edit');
    Route::get('/add', 'HomeController@add')->name('add');
    Route::get('/category','HomeController@category')->name('category');

    Route::post('/post/store', 'SaveController@savePost')->name('savePost');
    Route::post('/cate/store','SaveController@saveCategory')->name('saveCategory');
});

// 首页
Route::get('/', 'PostController@index');
// 详情页
Route::get('/{category}/{id}','PostController@detail');

