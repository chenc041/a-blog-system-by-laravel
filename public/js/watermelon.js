/**
 * 实例化富文本编辑器
 */
$(function (){
    var E = window.wangEditor;
    var editor = new E('#textArea');
    editor.customConfig.zIndex = 9999;
    editor.customConfig.uploadImgShowBase64 = true;
    editor.customConfig.onchange = function (html) {
        document.getElementById('content').value = html;
    };
    editor.create()
});
