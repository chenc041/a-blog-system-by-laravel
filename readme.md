## 一个简单的基于laravel 博客系统

#### 基于laravel开发
> laravel 版本 5.5

1.页面列表展示 完成

2.详情页展示 完成

3.后台管理登录/登出 完成

4.文章编辑/新增 完成

> 前端展示预览

##### 首页
![ALT TEXT](./screenshots/index.png "首页")
##### 详情页
![ALT TEXT](./screenshots/detail.png "详情页")
#### 登录/退出
![ALT TEXT](./screenshots/login.gif "退出/登录")


#### 安装使用

```

1. git clone https://git.oschina.net/WatermeLonMan/a-blog-system-by-laravel.git # 下载资源
2. composer install #使用composer 安装框架文件和所需的依赖文件
3. php artisan migrate:generate #使用migrate 根据已存在的数据表生成所有的表结构
4. php artisan migrate:generate tableName #使用 migrate 生成指定的表结构
```

#### 关于数据库迁移 
> 所用的 migrate 是来源于 https://github.com/Xethron/migrations-generator，
特点是可以根据已存在的数据库库生成migrations文件，命令是： php artisan migrate:generate
更加详细使用方法，请移步其git仓库查看

#### 关于 migrations-generator 插件
##### 如果在使用过程中出现以下问题
```
 Method Xethron\MigrationsGenerator\MigrateGenerateCommand::handle() does not exist
```
请修改位于以下路径中的文件代码（注：本程序已经解决该问题）：
> vendor\Xethron\migrations-generator\src\Xethron\MigrationsGenerator 中的 MigrateGeneratorCommand.php 
将 fire() 中的方法中的代码复制一份，然后复制到新建的 handle() 方法中即可

## License

The Blog System by Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
