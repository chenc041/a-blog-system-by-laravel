<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\CategoryRepository;
use App\Repositories\PostRepository;
use App\Repositories\TagRepository;

class PostController extends Controller
{
    // The CategoryRepository instance
    protected $category;

    //The PostRepository instance
    protected $post;

    // The TagRepository instance
    protected $tag;

    /**
     * Create a new Posts instance
     * PostController constructor.
     * @param CategoryRepository $category
     * @param PostRepository $post
     * @param TagRepository $tag
     */
    public function __construct(
        CategoryRepository $category,
        PostRepository $post,
        TagRepository $tag
    )
    {
        $this->category = $category;
        $this->post = $post;
        $this->tag = $tag;
    }

    /**
     * 显示首页数据
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(){
        $category = $this->initCate();
        $tags = $this->initTag();
        $posts = $this->post->getIndex();
        return view('front.index',compact('category','tags','posts'));
    }

    /**
     * 显示详情页数据
     * @param $cate
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function detail($cate,$id){
        $category = $this->initCate();
        $tags = $this->initTag();
        $article = $this->post->getDetail($id);
        return view('front.detail',compact('category','name','article','tags'));
    }

    /**
     * 初始化所有分类
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function initCate(){
        return $category = $this->category->getAllCategory();
    }

    /**
     * 初始化标签
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function initTag(){
        return $this->tag->getAllTags();
    }
}
