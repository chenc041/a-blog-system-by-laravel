<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\PostRepository;
use App\Repositories\CategoryRepository;

/**
 * for show pages
 * Class HomeController
 * @package App\Http\Controllers\Admin
 */
class HomeController extends Controller
{
    // The PostRepository instance
    protected $post;
    // The CategoryRepository instance
    protected $category;
    /**
     * Create a new controller instance.
     * HomeController constructor.
     * @param PostRepository $post
     * @param CategoryRepository $category
     */
    public function __construct(PostRepository $post,CategoryRepository $category)
    {
        $this->post = $post;
        $this->category = $category;
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $postList = $this->post->getPosts();
        return view('back.home',compact('postList'));
    }

    /**
     * Show the application edit page
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id){
        $postDetail = $this->post->getDetail($id);
        $category = $this->category->getAllCategory();
        return view('back.edit',compact('postDetail','id','category'));
    }

    /**
     * Show the add post page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add() {
        $category = $this->category->getAllCategory();
        return view('back.add',compact('category'));
    }

    /**
     * Show the add category page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function category(){
        return view('back.cate');
    }
}
