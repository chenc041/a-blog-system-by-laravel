<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\PostRepository;
use App\Repositories\CategoryRepository;

use App\Models\Tag;
/**
 * for save all data
 * Class SaveController
 * @package App\Http\Controllers\Admin
 */
class SaveController extends Controller
{
    // the PostRepository instance
    protected $post;
    // the CategoryRepository instance
    protected $category;

    /**
     * create new post and category instance
     * SaveController constructor.
     * @param PostRepository $post
     * @param CategoryRepository $category
     */
    public function __construct(PostRepository $post, CategoryRepository $category)
    {
        $this->post = $post;
        $this->category = $category;
        $this->middleware('auth');
    }

    /**
     * 保存或者更新文章
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function savePost(Request $request){
        $this->post->store($request->all(),$request->input('id'));
        return redirect('/home');
    }

    /**
     * 保存或者更新导航分类
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function saveCategory(Request $request){
        $this->category->store($request->all(),$request->input('id'));
        return redirect('/home');
    }
}
