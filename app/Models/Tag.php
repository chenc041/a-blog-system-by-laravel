<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tag extends Model
{
    protected $fillable = ['name','label','post_id'];
    // model related
    public function Post(){

        return $this->belongsTo('App\Models\Post');
    }
}
