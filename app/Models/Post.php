<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
class Post extends Model
{
    //model related Tag hasMany
    public function Tags(){
        return $this->hasMany('App\Models\Tag');
    }

    //model related Category belongsTo
    public function Category(){
        return $this->belongsTo('App\Models\Category');
    }
}
