<?php
namespace App\Repositories;

use App\Models\Category;

class CategoryRepository extends BaseRepository {
    /**
     * The Category instance
     * @var App\Models\Category
     */
    protected $category;

    /**
     * create a new category instance
     * CategoryRepository constructor.
     * @param Category $category
     */
    public function __construct(Category $category)
    {
        $this->category = $category;
    }

    /**
     * 获取所有分类
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAllCategory(){
        return $this->category->get();
    }



    public function store($cate,$id = false){


    }

    public function saveCate($model,$cate,$id){
        if($id){
            $cate = $model::find($id);
        }
    }
}