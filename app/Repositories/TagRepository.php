<?php
namespace App\Repositories;

use App\Models\Tag;
class TagRepository extends BaseRepository{
    // the tag instance
    protected $tag;

    /**
     * Create a new TagRepository instance
     * TagRepository constructor.
     * @param Tag $tag
     */
    public function __construct(Tag $tag)
    {
        $this->tag = $tag;
    }

    /**
     * 获取所有的tag
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAllTags(){
        return $this->tag->get();
    }
}