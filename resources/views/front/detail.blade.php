@extends('layouts.front')

@section('title','watermelon '.$article->name)
@section('keyword', 'watermelonBlog')
@section('description', 'watermelonBlog')
@section('content')
    <div class="water_main_content">
        <div class="water_content_layout">
            <div class="water_content_title">
                <h1>
                    {{$article->title}}
                </h1>
            </div>
            <div class="water_content_tips text-center">
                <span class="water_publish_time">
                    <i class="fa fa-calendar" aria-hidden="true"></i>
                    {{$article->created_at}}
                </span>
                <span class="water_article_comment text-center">
                    <i class="fa fa-hand-peace-o" aria-hidden="true"></i>
                    {{$article->view_count}}
                </span>
                <span class="water_article_category text-center">
                    <i class="fa fa-folder-o" aria-hidden="true"></i>
                    <a href="/{{$article->category->label}}">
                        {{$article->category->name}}
                    </a>
                </span>
            </div>
            <div class="water_article_detail">
                {!! $article->content !!}
            </div>
            <div class="water_article_bottom water_clear_fix">
                <div class="water_article_tags pull-left">
                    <i class="fa fa-tags" aria-hidden="true"></i>
                    @foreach($article->tags as $tag)
                        <a href="javascript:void(0)">
                            {{$tag->name}}
                        </a>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection