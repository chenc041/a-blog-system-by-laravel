@extends('layouts.front')

@section('title', 'watermelon Blog')
@section('keyword', 'watermelonBlog')
@section('description', 'watermelonBlog')

@section('content')
    @foreach ($posts as $item)
        <div class="water_main_content">
            <div class="water_content_layout">
                <div class="water_content_title">
                    <h1>
                        <a href="{{$item->id}}">
                            {{$item->title}}
                        </a>
                    </h1>
                </div>
                <div class="water_content_tips text-center">
                    <span class="water_publish_time">
                        <i class="fa fa-calendar" aria-hidden="true"></i>
                        {{$item->created_at}}
                    </span>
                        <span class="water_article_comment text-center">
                        <i class="fa fa-hand-peace-o" aria-hidden="true"></i>
                            {{$item->view_count}}
                    </span>
                     <span class="water_article_category text-center">
                        <i class="fa fa-folder-o" aria-hidden="true"></i>
                        <a href="{{$item->category->name}}">
                            {{$item->category->name}}
                        </a>
                    </span>
                </div>
                <div class="water_article_summary">
                    {{$item->summary}}
                </div>
                <div class="water_article_bottom water_clear_fix">
                    <div class="water_article_tags pull-left">
                        <i class="fa fa-tags" aria-hidden="true"></i>
                        @foreach($item->tags as $tag)
                            <a href="javascript:void(0)">
                                {{$tag->name}}
                            </a>
                        @endforeach
                    </div>
                    <div class="water_article_read pull-right">
                        <a href="{{$item->category->label}}/{{$item->id}}">
                            查看更多
                        </a>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
    @if($posts->lastPage() > 1)
        <div class="water_page_divider water_clear_fix">
            @if($posts->previousPageUrl() === null)
                <button class="water_prev_page pull-left water_page_btn" disabled>
                    上一页
                </button>
            @else
                <a href="{{$posts->previousPageUrl()}}">
                    <div class="water_prev_page pull-left water_page_btn">
                        上一页
                    </div>
                </a>
            @endif
            <a href="{{$posts->nextPageUrl()}}">
                <div class="water_next_page pull-right water_page_btn">
                     下一页
                </div>
            </a>
        </div>
    @endif
@endsection