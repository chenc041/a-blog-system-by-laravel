@extends('layouts.app')

@section('title', 'login | watermelon Blog System')
@section('keyword', 'watermelonBlog')
@section('description', 'watermelonBlog')

@section('content')
<div class="login_wrap">
    <form class="form-horizontal" method="POST" action="{{ route('login') }}" autocomplete="on">
        {{ csrf_field() }}

        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            <label for="email">邮箱地址</label>

            <div class="input_wrap">
                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus autocomplete="off">

                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
            <label for="password">密码</label>
            <div class="input_wrap">
                <input id="password" type="password" class="form-control" name="password" required autocomplete="off">
                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="login_btn_wrap">
            <button type="submit" class="btn">
                登录
            </button>
            <a class="btn-link" href="{{ route('password.request') }}">
                忘记密码?
            </a>
        </div>
    </form>
</div>
<canvas></canvas>
@endsection
