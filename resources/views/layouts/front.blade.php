<!doctype html>
<html lang="zh-cn">
<head>
    <meta charset="UTF-8">
    <title>@yield('title') | watermelon 的技术博客 | 前端 js vue php</title>
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta name="keywords" content="vue,javascript,js,css3,html5,设计,php,linux,cakePhp,laravel,node,linux,hexo轻博客,前端,前端开发,用户体验," />
    <meta name="description" content="watermelonman , 西瓜大侠, 前端, 设计, Hexo主题, 前端开发, 用户体验, 设计, frontend, design, nodejs, JavaScript, php, vue" />
    <link rel="stylesheet" href="https://cdn.bootcss.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="shortcut icon" href="{{asset('/favicon.ico')}}">
    <link rel="stylesheet" href="{{asset('/css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('/css/watermelonMan.css')}}">
</head>
<body>
<header id="water_header">
    <div class="water_header_top water_clear_fix">
        <div class="water_user_icon">
            <img src="{{asset('/images/avatar.jpg')}}" alt="头像">
        </div>
        <p class="water_intro">I am watermelonMan</p>
    </div>
    <div class="water_header_list">
        <div class="water_header_list_wrap">
            <div class="water_header_left_list">
                <a href="/">
                    <i class="fa fa-home" aria-hidden="true"></i>
                    首页
                </a>
                @foreach ($category as $item)
                    <a href="/{{$item->label}}">
                        {!! $item->icon !!}
                        {{$item->name}}
                    </a>
                @endforeach
            </div>
            <div class="water_right_right_form">
                <form action="" method="post">
                    <i class="fa fa-search" aria-hidden="true"></i>
                    <input type="text" class="water_search_input" placeholder="请输入查询内容">
                    <span class="water_search_btn"><i class="icon iconfont icon-search"></i></span>
                </form>
            </div>
        </div>
    </div>
</header>
<main class="water_main water_clear_fix">
    <div class="water_left_content">
        @yield('content')
    </div>
    <div class="water_sidebar">
        <div class="water_sidebar_header">
            <h3>站点Tags</h3>
        </div>
        @foreach ($tags as $item)
            <span>
                {{$item->name}}
            </span>
        @endforeach
    </div>
</main>
<footer id="water_footer">
    <div class="water_footer_content">
        Copyright &copy; 2017 watermelonMan
    </div>
</footer>
</body>
</html>