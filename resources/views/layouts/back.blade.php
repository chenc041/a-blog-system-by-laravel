<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>

    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <!-- Custom style -->
    <link rel="stylesheet" href="https://cdn.bootcss.com/material-design-iconic-font/2.2.0/css/material-design-iconic-font.min.css">
    <link rel="stylesheet" href="{{asset('lib/style/vivify.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/watermelon.css')}}">
</head>
<body>
<div id="app">
    <div class="wrapper">
        <div class="water_admin_header water_clear_fix">
            <div class="water_admin_logo water_clear_fix">
                <div class="water_admin_icon">
                    <img src="{{asset('/images/avatar.jpg')}}" alt="头像">
                </div>
                <div class="water_admin_intro">
                    {{ Auth::user()->name }}
                </div>
            </div>
            <div class="water_admin_nav_bar">
                <i class="zmdi zmdi-menu zmdi-hc-2x"></i>
            </div>
            <div class="water_admin_user_info vivify">
                <div class="water_admin_user_icon">
                    <img src="{{asset('images/avatar.jpg')}}" alt="头像">
                </div>
                <div class="water_admin_user_name">
                    {{ Auth::user()->name }}
                </div>
                <div class="water_admin_user_logout">
                    <a href="{{ route('logout') }}" class="water_admin_logout_btn"
                       onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                        退出
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </div>
            </div>
        </div>
        <div class="water_admin_main_side">
            <a href="/home" class="{{Route::currentRouteName() === 'home'?'sidebar_select':''}}">
                文章列表
            </a>
            <a href="/add" class="{{Route::currentRouteName() === 'add'?'sidebar_select':''}}">
                撰写文章
            </a>
            <a href="/category" class="{{Route::currentRouteName() === 'category'?'sidebar_select':''}}">
                添加分类
            </a>
        </div>
        <div class="water_main_wrap">
            @yield('content')
        </div>
    </div>

</div>

<!-- Scripts -->
<script src="{{asset('lib/javascript/jquery.3.2.1.min.js')}}"></script>
@stack('scripts')
<script>
    window.onload = function(){
        /**
         * 切换退出按钮显示状态
         * @type {Element}
         */
        var water_admin_user_info = document.querySelector('.water_admin_user_info');
        var water_admin_nav_bar = document.querySelector('.water_admin_nav_bar');
        water_admin_nav_bar.addEventListener('click',function(){
            if(water_admin_user_info.classList.contains('popInLeft')){
                water_admin_user_info.classList.add('popOutLeft');
                water_admin_user_info.classList.remove('popInLeft');
            } else {
                water_admin_user_info.classList.add('popInLeft');
                water_admin_user_info.classList.remove('popOutLeft');
            };
        });
    };
</script>
</body>
</html>
