@extends('layouts.back')
@section('title','新建导航分类 | watermelon')
@section('keyword', 'watermelonBlog')
@section('description', 'watermelonBlog')
@section('content')
    <div class="water_admin_content">
        <form action="/cate/store" method="post">
            <input name="_token" type="hidden" value="{{ csrf_token() }}">
            <div class="water_admin_content_title">
                <label for="title">分类名称 <span>*</span></label>
                <input type="text" name="cateName" id="title" placeholder="请输入分类名称">
            </div>
            <div class="water_admin_content_title">
                <label for="view_count">分类标签 <span>*</span></label>
                <input type="text" name="cateLabel" id="view_count" placeholder="请输入分类英文名称">
            </div>
            <div class="water_admin_content_title">
                <label for="view_count">分类icon</label>
                <input type="text" name="cateLabel" id="view_count" placeholder="请输入分类icon/cssFont">
            </div>
            <div class="water_admin_content_title">
                <label for="status">分类状态 <span>*</span></label>
                <input type="radio" name="status" id="status1" value="0" checked> 正常 <br>
                <input type="radio" name="status" id="status" value="1"> 已删除 <br>
            </div>
            <button type="submit" class="storeArticle">保存</button>
        </form>
    </div>
@endsection