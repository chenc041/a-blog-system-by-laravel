@extends('layouts.back')
@section('title','文章列表 | watermelon')
@section('keyword', 'watermelonBlog')
@section('description', 'watermelonBlog')
@section('content')
    <div class="water_admin_content">
        <table class="table water_admin_post_list">
            <thead>
            <tr>
                <th>序号</th>
                <th>分类</th>
                <th>标题</th>
                <th>浏览次数</th>
                <th>状态</th>
                <th>标签</th>
                <th>创建时间</th>
                <th>操作</th>
            </tr>
            </thead>
            <tbody>
            @foreach($postList as $item)
                <tr>
                    <td>{{$item->id}}</td>
                    <td>{{$item->category->name}}</td>
                    <td>{{$item->title}}</td>
                    <td>{{$item->view_count}}次</td>
                    @if($item->status === 0)
                    <td><span class="water_post_status_normal">正常</span></td>
                    @else
                    <td><span class="water_post_status_delete">已删除</span></td>
                    @endif
                    <td>
                        @foreach($item->tags as $value)
                            <span class="water_post_tag">{{$value->name}}</span>
                        @endforeach
                    </td>
                    <td>{{$item->created_at}}</td>
                    <td class="water_operate_btn">
                        <a href="/edit/{{$item->id}}" class="water_post_edit">编辑</a>
                        <a href="/delete/{{$item->id}}" class="water_post_delete">删除</a>
                        <a href="/{{$item->category->label}}/{{$item->id}}" class="water_post_check">查看</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
