@extends('layouts.back')
@section('title','新建文章 | watermelon')
@section('keyword', 'watermelonBlog')
@section('description', 'watermelonBlog')
@section('content')
    <div class="water_admin_content">
        <form action="/post/store" method="post">
            <input name="_token" type="hidden" value="{{ csrf_token() }}">
            <div class="water_admin_content_title">
                <label for="title">文章标题 <span>*</span></label>
                <input type="text" name="title" id="title" placeholder="请输入文章标题">
            </div>
            <div class="water_admin_content_title">
                <label for="view_count">浏览次数 <span>*</span></label>
                <input type="text" name="view_count" id="view_count" placeholder="请输入文章浏览次数">
            </div>
            <div class="water_admin_content_title">
                <label for="status">文章状态 <span>*</span></label>
                <input type="radio" name="status" id="status1" value="0" checked> 正常 <br>
                <input type="radio" name="status" id="status" value="1"> 已删除 <br>
            </div>
            <div class="water_admin_cate">
                <label for="cate">文章分类 <span>*</span></label>
                <select name="category" id="cate">
                    <option value="">请选择文章分类</option>
                    @foreach($category as $item)
                        <option value="{{$item->id}}">{{$item->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="water_admin_content_tag">
                <label for="tags">文章标签 <span>*</span></label>
                <input type="text" name="tag" id="tags" placeholder="请输入文章标签，以空格间隔">
            </div>
            <div class="water_admin_content_summary">
                <label for="summary">文章摘要 <span>*</span></label>
                <textarea name="summary" id="summary" cols="30" rows="5" placeholder="请输入文章摘要"></textarea>
            </div>
            <div class="water_admin_main_content">
                <label for="textArea">文章内容 <span>*</span></label>
                <input name="content" id="content" type="hidden">
                <div id="textArea">
                    <p>请输入文章标题...</p>
                </div>
            </div>
            <button type="submit" class="storeArticle">保存</button>
        </form>
    </div>
@endsection
@push('scripts')
    <script src="{{asset('lib/javascript/wangEditor.min.js')}}"></script>
    <script src="{{asset('js/watermelon.js')}}"></script>
@endpush