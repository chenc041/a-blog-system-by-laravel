<?php
/**
 * @var $postDetail
 */
?>
@extends('layouts.back')
@section('title', $postDetail->title.' | watermelon')
@section('keyword', 'watermelonBlog')
@section('description', 'watermelonBlog')
@section('content')
    <div class="water_admin_content">
        <form action="/post/store" method="post">
            <input name="_token" type="hidden" value="{{ csrf_token() }}">
            <input name="id" type="hidden" value="{{$postDetail->id}}">
            <div class="water_admin_content_title">
                <label for="title">文章标题 <span>*</span></label>
                <input type="text" name="title" id="title" value="{{$postDetail->title}}">
            </div>
            <div class="water_admin_content_title">
                <label for="view_count">浏览次数 <span>*</span></label>
                <input type="text" name="view_count" id="view_count" value="{{$postDetail->view_count}}">
            </div>
            <div class="water_admin_content_title">
                <label for="status">文章状态 <span>*</span></label>
                <input type="radio" name="status" id="status" value="0" {{$postDetail->status === 0?'checked':''}}> 正常 <br>
                <input type="radio" name="status" id="status" value="1" {{$postDetail->status === 1?'checked':''}}> 已删除 <br>
            </div>
            <div class="water_admin_cate">
                <label for="cate">文章分类 <span>*</span></label>
                <select name="category">
                    @foreach($category as $item)
                        <option value="{{$item->id}}" {{($postDetail->category->id === $item->id)?"selected":''}}>{{$item->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="water_admin_content_tag">
                <label for="tags">文章标签 <span>*</span></label>
                <input type="text" name="tag" id="tags" value="@foreach($postDetail->tags as $item){{$item->name.','}}@endforeach">
            </div>
            <div class="water_admin_content_summary">
                <label for="summary">文章摘要 <span>*</span></label>
                <textarea name="summary" id="summary" cols="30" rows="5">{{$postDetail->summary}}</textarea>
            </div>
            <div class="water_admin_main_content">
                <label for="content">文章内容 <span>*</span></label>
                <input name="content" id="content" type="hidden" value="{{$postDetail->content}}">
                <div id="textArea">
                    {!! $postDetail->content !!}
                </div>
            </div>
            <button type="submit" class="storeArticle">保存</button>
        </form>
    </div>
@endsection
@push('scripts')
    <script src="{{asset('lib/javascript/wangEditor.min.js')}}"></script>
    <script src="{{asset('js/watermelon.js')}}"></script>
@endpush